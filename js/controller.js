(function () {
  console.log('controller loaded')

  var Controller = function ($scope) {
    console.log($scope)
  }

  Controller.$inject = ['$scope']

  angular.module('myApp')
    .controller('Controller', Controller)
}())